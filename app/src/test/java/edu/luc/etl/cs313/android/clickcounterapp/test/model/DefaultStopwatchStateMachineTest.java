//package edu.luc.etl.cs313.android.clickcounterapp.test.model;
//
//import org.junit.After;
//import org.junit.Before;
//
//import edu.luc.etl.cs313.android.clickcounterapp.model.state.DefaultClickCounterStateMachine;
//
///**
// * Concrete testcase subclass for the default stopwatch state machine
// * implementation.
// *
// * @author laufer
// * @see <a href="http://xunitpatterns.com/Testcase%20Superclass.html">...</a>
// */
//public class DefaultStopwatchStateMachineTest extends AbstractStopwatchStateMachineTest {
//
//    @Before
//    public void setUp() throws Exception {
//        super.setUp();
//        setModel(new DefaultClickCounterStateMachine(getDependency(), getDependency()));
//    }
//
//    @After
//    public void tearDown() {
//        setModel(null);
//        super.tearDown();
//    }
//}
