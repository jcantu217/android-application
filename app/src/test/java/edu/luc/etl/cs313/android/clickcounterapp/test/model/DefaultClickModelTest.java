package edu.luc.etl.cs313.android.clickcounterapp.test.model;

import org.junit.After;
import org.junit.Before;

import edu.luc.etl.cs313.android.clickcounterapp.model.click.DefaultClickModel;

/**
 * Concrete testcase subclass for the default time model implementation.
 *
 * @author laufer
 * @see <a href="http://xunitpatterns.com/Testcase%20Superclass.html">...</a>
 */
public class DefaultClickModelTest extends AbstractClickModelTest {

    @Before
    public void setUp() throws Exception {
        setModel(new DefaultClickModel());
    }

    @After
    public void tearDown() throws Exception {
        setModel(null);
    }
}
