package edu.luc.etl.cs313.android.clickcounterapp.test.model;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import edu.luc.etl.cs313.android.clickcounterapp.model.click.ClickModel;

/**
 * Testcase superclass for the time model abstraction.
 * This is a simple unit test of an object without dependencies.
 *
 * @author laufer
 * @see <a href="http://xunitpatterns.com/Testcase%20Superclass.html">...</a>
 */
public abstract class AbstractClickModelTest {

    private ClickModel model;

    /**
     * Setter for dependency injection. Usually invoked by concrete testcase
     * subclass.
     *
     * @param model
     */
    protected void setModel(final ClickModel model) {
        this.model = model;
    }

    /**
     * Verifies that runtime and laptime are initially 0 or less.
     */
    @Test
    public void testPreconditions() {
        assertEquals(0, model.getValue());
    }

    /**
     * Verifies that runtime is incremented correctly.
     */
    @Test
    public void testIncrementRuntimeOne() {
        final var rt = model.getValue();
        model.inc();
        assertEquals(rt, model.getValue());
    }
}
