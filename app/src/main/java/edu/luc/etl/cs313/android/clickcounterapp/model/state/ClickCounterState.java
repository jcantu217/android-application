package edu.luc.etl.cs313.android.clickcounterapp.model.state;

import edu.luc.etl.cs313.android.clickcounterapp.common.ClickCounterUIListener;
import edu.luc.etl.cs313.android.clickcounterapp.model.clock.TickListener;

/**
 * A state in a state machine. This interface is part of the State pattern.
 *
 * @author laufer
 */
interface ClickCounterState extends ClickCounterUIListener, TickListener {
    void updateView();
    int getId();
}
