package edu.luc.etl.cs313.android.clickcounterapp.common;

/**
 * A stoppable component.
 *
 * @author laufer
 */
public interface Stoppable {
    void stop();
}
