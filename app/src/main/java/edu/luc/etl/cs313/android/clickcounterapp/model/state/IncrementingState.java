package edu.luc.etl.cs313.android.clickcounterapp.model.state;

import edu.luc.etl.cs313.android.clickcounterapp.R;
import java.util.Timer;
import java.util.TimerTask;

/*
 * This is the incrementing state for the state machine. This is where the functionality of the button click sticks to
 * incrementing until the timeout changes its state to running
 */
class IncrementingState implements ClickCounterState {

    public IncrementingState(final ClickCounterSMStateView sm) {
        this.sm = sm;
    }

    private final ClickCounterSMStateView sm;

    private Timer timer = new Timer();

    /*
     * using java's Timer interface, it will help time the timeout needed to jump to the running state
     */
    @Override
    public void onClick() {
        this.timer.cancel();
        sm.actionInc();
        sm.toIncrementState();
        if (sm.actionIfFull()) {
            sm.actionStart();
            sm.toRunningState();
        } else {
            this.timer = new Timer();
            this.timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    timer.cancel();
                    sm.actionStart();
                    sm.toRunningState();
                }
            },3000);
        }
    }

    @Override
    public void onTick() {
        throw new UnsupportedOperationException("onTick");
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.INCREMENTING;
    }
}
