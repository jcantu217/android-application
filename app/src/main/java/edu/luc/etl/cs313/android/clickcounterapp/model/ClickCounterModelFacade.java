package edu.luc.etl.cs313.android.clickcounterapp.model;

import edu.luc.etl.cs313.android.clickcounterapp.common.Startable;
import edu.luc.etl.cs313.android.clickcounterapp.common.ClickCounterModelSource;
import edu.luc.etl.cs313.android.clickcounterapp.common.ClickCounterUIListener;

/**
 * A thin model facade. Following the Facade pattern,
 * this isolates the complexity of the model from its clients (usually the adapter).
 *
 * @author laufer
 */
public interface ClickCounterModelFacade extends Startable, ClickCounterUIListener, ClickCounterModelSource { }
