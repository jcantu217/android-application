package edu.luc.etl.cs313.android.clickcounterapp.common;

//todo towards the end of the project, we can delete onLapReset() as there will be one button
/**
 * A listener for stopwatch events coming from the UI.
 *
 * @author laufer
 */
public interface ClickCounterUIListener {
    void onClick();
}
