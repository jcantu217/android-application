package edu.luc.etl.cs313.android.clickcounterapp.android;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import java.util.Locale;
import edu.luc.etl.cs313.android.clickcounterapp.R;
import edu.luc.etl.cs313.android.clickcounterapp.common.ClickCounterModelListener;
import edu.luc.etl.cs313.android.clickcounterapp.model.ConcreteClickCounterModelFacade;
import edu.luc.etl.cs313.android.clickcounterapp.model.ClickCounterModelFacade;

/**
 * A thin adapter component for the stopwatch.
 *
 * @author laufer
 */
public class ClickCounterAdapter extends Activity implements ClickCounterModelListener {

    private static String TAG = "click-countdown-activity";

    /**
     * The state-based dynamic model.
     */
    private ClickCounterModelFacade model;

    protected void setModel(final ClickCounterModelFacade model) {
        this.model = model;
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // inject dependency on view so this adapter receives UI events
        setContentView(R.layout.activity_main);
        // inject dependency on model into this so model receives UI events
        this.setModel(new ConcreteClickCounterModelFacade());
        // inject dependency on this into model to register for UI updates
        model.setModelListener(this);

        // TODO reads user input if submitted but does not update counter value
//        TextView inputTest = (TextView) findViewById(R.id.testView);
        Button submitButton = (Button) findViewById(R.id.Submit);
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText inputBox = (EditText) findViewById(R.id.userInput);
                if (inputBox != null) {
                    String stringContent = inputBox.getText().toString();
                    int userValue = Integer.parseInt(stringContent);
//                    inputTest.setText(userValue + "");
                }
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    protected void onStart() {
        super.onStart();
        model.start();
    }

    /**
     * Updates the timer in the UI
     * @param countdown
     */
    public void onCountdown(final int countdown) {
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(() -> {
            final TextView tvS = findViewById(R.id.countdown);
            final var locale = Locale.getDefault();
            tvS.setText(String.format(locale,"%02d", countdown));
        });
    }

    /**
     * Updates the state name in the UI.
     * @param stateId
     */
    public void onStateUpdate(final int stateId) {
        Uri alarm = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_ALARM);
        Ringtone a = RingtoneManager.getRingtone(this,alarm);
        // UI adapter responsibility to schedule incoming events on UI thread
        runOnUiThread(() -> {
            final TextView stateName = findViewById(R.id.stateName);
            stateName.setText(getString(stateId));
            //beep to notify that the timer has started
            if (stateName.getText().equals("Running")) {
                onBeep();
            }
            if (stateName.getText().equals("Ringing")) {
                //a.play(); could not get to stop ringing even though it was not in the ringing state anymore.
            }
        });
    }

    public void onBeep() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        Ringtone n = RingtoneManager.getRingtone(this,notification);
        n.play();
    }

    // forward event listener methods to the model
    public void onClick(final View view) {
        model.onClick();
    }
}