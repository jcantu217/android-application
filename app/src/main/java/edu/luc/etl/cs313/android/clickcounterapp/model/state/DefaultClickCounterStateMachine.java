package edu.luc.etl.cs313.android.clickcounterapp.model.state;

import edu.luc.etl.cs313.android.clickcounterapp.common.ClickCounterModelListener;
import edu.luc.etl.cs313.android.clickcounterapp.model.clock.ClockModel;
import edu.luc.etl.cs313.android.clickcounterapp.model.click.ClickModel;

/**
 * An implementation of the state machine for the stopwatch.
 *
 * @author laufer
 */
public class DefaultClickCounterStateMachine implements ClickCounterStateMachine {

    public DefaultClickCounterStateMachine(final ClickModel clickModel, final ClockModel clockModel) {
        this.clickModel = clickModel;
        this.clockModel = clockModel;
        // TODO add onclick to reset timer based on user input by checking if there is an int value in TextView box
    }

    private final ClickModel clickModel;

    private final ClockModel clockModel;

    /**
     * The internal state of this adapter component. Required for the State pattern.
     */
    private ClickCounterState state;

    protected void setState(final ClickCounterState state) {
        this.state = state;
        listener.onStateUpdate(state.getId());
    }

    private ClickCounterModelListener listener;

    @Override
    public void setModelListener(final ClickCounterModelListener listener) {this.listener = listener;}

    // forward event uiUpdateListener methods to the current state
    // these must be synchronized because events can come from the
    // UI thread or the timer thread
    @Override public synchronized void onClick() {state.onClick();}
    @Override public synchronized void onTick() {state.onTick();
    }

    @Override public void updateUIRuntime() { listener.onCountdown(clickModel.getValue()); }

    // known states
    private final ClickCounterState STOPPED     = new StoppedState(this);
    private final ClickCounterState RUNNING     = new RunningState(this);
    private final ClickCounterState INCREMENTING = new IncrementingState(this);
    private final ClickCounterState ALARMING = new RingingState(this);

    // transitions
    @Override public void toRunningState()    { setState(RUNNING); }
    @Override public void toStoppedState()    { setState(STOPPED); }
    @Override public void toIncrementState() {setState(INCREMENTING);}
    @Override public void toAlarmingState() {setState(ALARMING);}

    // actions
    @Override public void actionInit() {toStoppedState(); actionReset();}
    @Override public void actionReset() {clickModel.reset(); actionUpdateView();}
    @Override public void actionStart() {clockModel.start();}
    @Override public void actionStop() {clockModel.stop();}
    @Override public void actionInc() {clickModel.inc(); actionUpdateView();}
    @Override public void actionDec() {clickModel.dec(); actionUpdateView();}
    @Override public boolean actionIfFull() {return clickModel.isFull();} //helps by checking if the clickModel is empty
    @Override public boolean actionIfEmpty() {return clickModel.isEmpty();} //of if it's full
    @Override public void actionUpdateView() {state.updateView();}
}