package edu.luc.etl.cs313.android.clickcounterapp.model.click;

/**
 * The passive data model of the stopwatch.
 * It does not emit any events.
 *
 * @author laufer
 */
public interface ClickModel {
    void reset();
    void inc();
    void dec();
    int getValue();
    boolean isFull();
    boolean isEmpty();
}
