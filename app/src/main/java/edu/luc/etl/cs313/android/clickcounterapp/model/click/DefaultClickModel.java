package edu.luc.etl.cs313.android.clickcounterapp.model.click;

/**
 * An implementation of the click counter data model. this design follows the click counter app from loyola chicago
 * code.
 */
public class DefaultClickModel implements ClickModel {

    private int value;
    private int max;
    private int min;
    public DefaultClickModel() {this(0,99);}

    public DefaultClickModel(int min, int max) {
        if (min >= max) {
            throw new IllegalArgumentException("min >= max");
        }
        this.min = min;
        this.max = max;
        this.value = min;
    }

    protected boolean dataInvariant() {return min <= value && value <= max;}

    @Override
    public void reset() {value = 0;}
    @Override
    public void inc() {
        if (!isFull() && dataInvariant())
            ++value;
    }
    @Override
    public void dec() {
        if (!isEmpty() && dataInvariant())
            --value;
    }
    @Override
    public int getValue() {return value;}
    @Override
    public boolean isFull() {return value >= max;}
    @Override
    public boolean isEmpty() {return value <= min;}


}