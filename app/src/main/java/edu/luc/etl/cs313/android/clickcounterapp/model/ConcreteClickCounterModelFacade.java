package edu.luc.etl.cs313.android.clickcounterapp.model;

import edu.luc.etl.cs313.android.clickcounterapp.common.ClickCounterModelListener;
import edu.luc.etl.cs313.android.clickcounterapp.model.clock.ClockModel;
import edu.luc.etl.cs313.android.clickcounterapp.model.clock.DefaultClockModel;
import edu.luc.etl.cs313.android.clickcounterapp.model.state.DefaultClickCounterStateMachine;
import edu.luc.etl.cs313.android.clickcounterapp.model.state.ClickCounterStateMachine;
import edu.luc.etl.cs313.android.clickcounterapp.model.click.DefaultClickModel;
import edu.luc.etl.cs313.android.clickcounterapp.model.click.ClickModel;

/**
 * An implementation of the model facade. This hides the complexity of the model with a simpler class for the client.
 */
public class ConcreteClickCounterModelFacade implements ClickCounterModelFacade {

    private final ClickCounterStateMachine stateMachine;

    private final ClockModel clockModel;

    private final ClickModel clickModel;

    public ConcreteClickCounterModelFacade() {
        clickModel = new DefaultClickModel();
        clockModel = new DefaultClockModel();
        stateMachine = new DefaultClickCounterStateMachine(clickModel, clockModel);
        clockModel.setTickListener(stateMachine);
    }

    @Override
    public void start() {
        stateMachine.actionInit();
    }

    @Override
    public void setModelListener(final ClickCounterModelListener listener) {
        stateMachine.setModelListener(listener);
    }

    @Override
    public void onClick() {
        stateMachine.onClick();
    }
}
