package edu.luc.etl.cs313.android.clickcounterapp.model.state;

import edu.luc.etl.cs313.android.clickcounterapp.common.ClickCounterModelSource;
import edu.luc.etl.cs313.android.clickcounterapp.common.ClickCounterUIListener;
import edu.luc.etl.cs313.android.clickcounterapp.model.clock.TickListener;

/**
 * The state machine for the state-based dynamic model of the stopwatch.
 * This interface is part of the State pattern.
 *
 * @author laufer
 */
public interface ClickCounterStateMachine extends ClickCounterUIListener, TickListener, ClickCounterModelSource,
        ClickCounterSMStateView { }
