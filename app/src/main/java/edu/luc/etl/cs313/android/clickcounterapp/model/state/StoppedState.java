package edu.luc.etl.cs313.android.clickcounterapp.model.state;

import edu.luc.etl.cs313.android.clickcounterapp.R;

/**
 * This is the stopped state for the state machine. This will be the state where the client can increment the value
 * to a certain point and how long the timer will go for.
 */
class StoppedState implements ClickCounterState {


    public StoppedState(final ClickCounterSMStateView sm) {
        this.sm = sm;
    }

    private final ClickCounterSMStateView sm;

   @Override
    public void onClick() {
        sm.actionInc();
        sm.toIncrementState();
    }

    @Override
    public void onTick() {
        sm.actionStop();
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.STOPPED;
    }
}
