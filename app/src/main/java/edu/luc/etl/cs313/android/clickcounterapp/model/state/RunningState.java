package edu.luc.etl.cs313.android.clickcounterapp.model.state;

import edu.luc.etl.cs313.android.clickcounterapp.R;

/*
 * This is the running state for the state machine. This is where the functionality of the button click changes from
 * incrementing to being a reset button.
 */
class RunningState implements ClickCounterState {

    public RunningState(final ClickCounterSMStateView sm) {
        this.sm = sm;
    }

    private final ClickCounterSMStateView sm;

    /*
     * button press now will reset the timer and put it back into the stopped state
     */
    @Override
    public void onClick() {
        sm.actionReset();
        sm.actionStop();
        sm.toStoppedState();
    }

    /*
     * decrements the counter every tick
     */
    @Override
    public void onTick() {
        if (!sm.actionIfEmpty()) {
            sm.actionDec();
        } else {
            sm.toAlarmingState();
        }
    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RUNNING;
    }
}
