package edu.luc.etl.cs313.android.clickcounterapp.model.state;

import edu.luc.etl.cs313.android.clickcounterapp.R;

/*
 * This is the alarming state for the state machine. This is where the timer beeps until it a button is clicked to shut
 * it off.
 */
class RingingState implements ClickCounterState {

    public RingingState(final ClickCounterSMStateView sm) {
        this.sm = sm;
    }

    private final ClickCounterSMStateView sm;

    /*
     * button press now will reset the timer and put it back into the stopped state
     */
    @Override
    public void onClick() {
        sm.actionStop();
        sm.toStoppedState();
        sm.actionReset();
    }

    /*
     * keeps ticking, though nothing happens. ideally would have this telling the alarm if the clock is still ticking
     */
    @Override
    public void onTick() {

    }

    @Override
    public void updateView() {
        sm.updateUIRuntime();
    }

    @Override
    public int getId() {
        return R.string.RINGING;
    }
}
