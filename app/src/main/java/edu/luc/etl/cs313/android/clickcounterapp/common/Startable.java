package edu.luc.etl.cs313.android.clickcounterapp.common;

/**
 * A startable component.
 *
 * @author laufer
 */
public interface Startable {
    void start();
}
