package edu.luc.etl.cs313.android.clickcounterapp.common;

/**
 * A listener for UI update events.
 * This interface is typically implemented by the adapter, with the
 * events coming from the model.
 *
 * @author laufer
 */
public interface ClickCounterModelListener {
    void onCountdown(int value);
    void onStateUpdate(int stateId);
    void onBeep();
}
