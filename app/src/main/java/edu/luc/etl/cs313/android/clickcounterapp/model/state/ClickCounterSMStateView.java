package edu.luc.etl.cs313.android.clickcounterapp.model.state;

/**
 * The restricted view states have of their surrounding state machine.
 * This is a client-specific interface in Peter Coad's terminology.
 *
 * @author laufer
 */
interface ClickCounterSMStateView {

    // transitions
    void toRunningState();
    void toStoppedState();
    void toIncrementState();
    void toAlarmingState();

    // actions
    void actionInit();
    void actionReset();
    void actionStart();
    void actionStop();
    void actionInc();
    void actionDec();
    void actionUpdateView();
    boolean actionIfFull();
    boolean actionIfEmpty();

    // state-dependent UI updates
    void updateUIRuntime();
}
